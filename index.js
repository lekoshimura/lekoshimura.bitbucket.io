[{
  "url": "css-position.html",
  "title": "CSS: position",
  "img": "img/cards/css-position-pb.png",
  "tags": ["css"]
},
{
  "url": "css-vertical-alignment.html",
  "title": "CSS: vertical-alignment",
  "img": "img/cards/css-vertical-alignment-pb.png",
  "tags": ["css"]
},
{
  "url": "css-flex-x-float.html",
  "title": "CSS: flex x float",
  "img": "img/cards/css-flex-x-float-pb.png",
  "tags": ["css", "flex"]
},
{
  "url": "css-flex-basis.html",
  "title": "CSS: flex-basis e flex-grow",
  "img": "img/cards/css-flex-basis-pb.png",
  "tags": ["css", "flex"]
},
{
  "url": "css-flex-justify-content.html",
  "title": "CSS: justify-content",
  "img": "img/cards/css-flex-justify-content-pb.png",
  "tags": ["css", "flex"]
},
{
  "url": "css-vertical-centering.html",
  "title": "CSS: Vertical Centering",
  "img": "img/cards/css-vertical-centering-pb.png",
  "tags": ["css"]
},
{
  "url": "css-image-fade.html",
  "title": "CSS: Image Fade In, Fade Out",
  "img": "img/cards/css-image-fade.png",
  "tags": ["css", "snippets"]
},
{
  "url": "css-activate-button.html",
  "title": "CSS: Activate Button",
  "img": "img/cards/css-activate-button.png",
  "tags": ["css", "snippets"]
},
{
  "url": "css-weird-heights.html",
  "title": "CSS: Weird Heights - Flexbox",
  "img": "img/cards/css-weird-heights.png",
  "tags": ["css", "weird"]
},
{
  "url": "img-caption.html",
  "title": "IMG: Caption",
  "img": "img/cards/img-caption.png",
  "tags": ["css", "img"]
},
{
  "url": "css-weird-gap.html",
  "title": "CSS: Weird Gap - Image",
  "img": "img/cards/css-weird-gap.png",
  "tags": ["img", "css", "weird"]
},
{
  "url": "docker-tomcat.html",
  "title": "Docker: Tomcat com server.xml modificado",
  "img": "img/cards/docker-tomcat.png",
  "tags": ["docker", "tomcat"]
},
{
  "url": "bulma-tabs-with-content.html",
  "title": "Bulma: Tabs with Content",
  "img": "img/cards/bulma-tabs-with-content.png",
  "tags": ["bulma", "snippets"]
},
{
  "url": "css-flexbox-grid-com-after.html",
  "title": "CSS: Flexbox Grid com pseudo-seletores ::after e :nth-child",
  "img": "img/cards/css-flexbox-grid-com-after.png",
  "tags": ["css", "flex"]
},
{
  "url": "img-nextgen-format.html",
  "title": "IMG: NextGen Formats Conversion",
  "img": "img/cards/img-nextgen-format.png",
  "tags": ["img"]
},
{
  "url": "img-responsiveness.html",
  "title": "IMG: Responsividade com <picture>",
  "img": "img/cards/img-responsiveness.png",
  "tags": ["img"]
},
{
  "url": "open-options-in-table.html",
  "title": "Abrir Linha de Opçõoes em Tabela",
  "img": "img/cards/open-options-in-table.png",
  "tags": ["snippet"]
},
{
  "url": "off-canvas.html",
  "title": "Off Canvas",
  "img": "img/cards/off-canvas.png",
  "tags": ["snippet"]
},
{
  "url": "js-intersection-observer-api.html",
  "title": "JS: Intersection Observer API",
  "img": "img/cards/js-intersection-observer-api.png",
  "tags": ["javascript"]
},
{
  "url": "loading.html",
  "title": "Loading (JS + CSS)",
  "img": "img/cards/loading.png",
  "tags": ["snippet"]
},
{
  "url": "loading-web-components.html",
  "title": "Loading (Web Components)",
  "img": "img/cards/loading.png",
  "tags": ["snippet", "web-components"]
},
{
  "url": "vscode-like-sidenav.html",
  "title": "Sidenav Responsivo Tipo VSCode",
  "img": "img/cards/vscode-like-sidenav.png",
  "tags": ["snippet"]
},
{
  "url": "vscode-like-sidenav-v2.html",
  "title": "Sidenav Responsivo Tipo VSCode com Scroll",
  "img": "img/cards/vscode-like-sidenav-v2.png",
  "tags": ["snippet"]
},
{
  "url": "css-badge.html",
  "title": "CSS Badge",
  "img": "img/cards/css-badge.png",
  "tags": ["snippet"]
},
{
  "url": "js-pie-countdown.html",
  "title": "JS: Countdown Pie",
  "img": "img/cards/js-pie-countdown.png",
  "tags": ["snippet"]
},
{
  "url": "bulma-dropdown-with-filtering.html",
  "title": "Bulma: Dropdown With Filtering",
  "img": "img/cards/bulma-dropdown-with-filtering.png",
  "tags": ["bulma", "snippets"]
},
{
  "url": "modal-image.html",
  "title": "Modal With Image (Web Components)",
  "img": "img/cards/modal-image.png",
  "tags": ["bulma", "snippets", "web-components"]
},
{
  "url": "frosted-background.html",
  "title": "Frosted Background",
  "img": "img/cards/frosted-background.png",
  "tags": ["snippets"]
},
{
  "url": "responsive-patterns.html",
  "title": "Responsive Web Design Patterns"
},
{
  "url": "responsive-tables.html",
  "title": "Responsive Tables"
},
{
  "url": "hero-with-flex.html",
  "title": "Hero With Flex"
}
]
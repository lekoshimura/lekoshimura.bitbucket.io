let eiji = {};

eiji.getParameterByName = function (name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return '';
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
};

eiji.checkdumpjson = function (stringfied) {
  if (eiji.getParameterByName('dumpjson') === 'true') {
    var w = window.open('', 'dumpjson', '_blank');
    w.document.body.innerHTML = w.document.body.innerHTML + '<textarea style="width:100%;height:300px">' + stringfied + '</textarea>';
    w.focus();
  }
};

eiji.ajaxWithPromise = function (fields, url, formType) {
  return new Promise(function (resolve, reject) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState === 4) {
        eiji.checkdumpjson(this.responseText);
        switch (this.status) {
          case 200:
            resolve(xhttp.response);
            break;
          default:
            if (this.responseText.length !== 0) {
              reject({
                eijiException: JSON.parse(this.responseText),
                status: this.status,
                statusText: xhttp.statusText
              });
            } else {
              let msg;
              if (xhttp.readyState == 4 && xhttp.status == 0) {
                msg = 'ERRO DESCONHECIDO: ERR_NAME_NOT_RESOLVED / ERR_CONNECTION_REFUSED / ERR_BLOCKED_BY_CLIENT / ERR_TUNNEL_CONNECTION_FAILED';
              } else {
                msg = xhttp.statusText
              }
              reject({
                status: this.status,
                statusText: msg
              });
            }
            break;
        }
      }
    };
    if ((formType === 'post') || (formType === 'POST')) {
      xhttp.open('POST', url);
      xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhttp.send(fields);
    } else {
      xhttp.open('GET', url + '?' + fields, true);
      xhttp.send();
    }
  });
};

eiji.arrayRotateOne = function (arr, reverse) {
  if (reverse)
    arr.unshift(arr.pop())
  else
    arr.push(arr.shift())
  return arr
};